using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class pitbull_controleFlames : spawnBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        waitingForNextSpawn = 1f;
        theCountdown = waitingForNextSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3((Input.GetAxis("Horizontal")*Time.deltaTime) + transform.position.x, transform.position.y, transform.position.z);
        xMin = transform.position.x;
        yMin = transform.position.y;
        if (xMin != 0)
        {
            theCountdown -= Time.deltaTime;
            if (theCountdown <= 0)
            {
                spawnStuff();
                theCountdown = waitingForNextSpawn;
            }
           
        }
    }
    private void spawnStuff()
    {
        Vector2 pos = new Vector2(xMin, yMin);
        GameObject objectToSpawn = Objects[0];
        Instantiate(objectToSpawn, pos, transform.rotation);

    }
}
