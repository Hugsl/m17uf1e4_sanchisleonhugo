using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KnightData : MonoBehaviour
{
    private bool win;
    protected float time;

    public enum state
    {
        idle,
        run,
        dash,
        death
    }

    public enum jumpstate
    {
        ground,
        jump,
        fall
    }

    public Animator animator;
    public float weight;
    public float speed;
    public Vector2 jumpHeight;
    public state estado;
    public jumpstate saltoestado;


    // Start is called before the first frame update
    void Start()
    {
        time = 60f;
        jumpHeight = new Vector2(0, 3);
        speed = 0.5f * Time.deltaTime;
        weight = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (time>0)
        {
            time -= Time.deltaTime;
            moving();
        }
        else
        {
            win = true;
            PlayerPrefs.SetString("win", win.ToString());
            SceneManager.LoadScene("FinalSceene");
        }
        
    }



    private void moving()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {

            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {

            transform.localRotation = Quaternion.Euler(0, 0, 0);

        }


        checkjump();
        if (saltoestado == jumpstate.jump) {
            GetComponent<Rigidbody2D>().AddForce(jumpHeight, ForceMode2D.Impulse);
        }
        transform.position = new Vector3((Input.GetAxis("Horizontal") * speed) + transform.position.x, transform.position.y, transform.position.z);
        animator.SetFloat("speed", Mathf.Abs(Input.GetAxis("Horizontal")));
    }
    private void checkjump()
    {
        if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && saltoestado == jumpstate.ground)
        {

            animator.SetBool("jump", true);
            animator.SetBool("ground", false);
            saltoestado = jumpstate.jump;
        }
        else if (saltoestado == jumpstate.jump && transform.position.y >= 0.8)
        {
            animator.SetBool("jump", false);
            animator.SetBool("fall", true);
            saltoestado = jumpstate.fall;
        }


    }
    private void stop()
    {   
        animator.SetBool("hitt",false);
    }
    private void die()
    {
        SceneManager.LoadScene("FinalSceene");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {




        if (collision.collider.CompareTag("Ground"))
        {

            animator.SetBool("fall", false);
            animator.SetBool("ground", true);
            saltoestado = jumpstate.ground;
        }

        if (collision.collider.CompareTag("DeathZone") && collision.otherCollider == this.GetComponent<Collider2D>())
        {
            win = false;
            PlayerPrefs.SetString("win",win.ToString());
           SceneManager.LoadScene("FinalSceene");

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeathZone"))
        {
            animator.SetBool("hitt", true);

        }
    }
}

