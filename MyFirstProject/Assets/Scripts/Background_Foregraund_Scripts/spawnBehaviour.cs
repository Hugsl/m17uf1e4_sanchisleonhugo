using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBehaviour : MonoBehaviour
{


    //https://answers.unity.com/questions/1110965/c-spawn-random-object-at-random-2d-location.html


    [SerializeField] public List<GameObject> Objects = new List<GameObject>();

    protected  float waitingForNextSpawn = 3;
    protected float theCountdown;


    public float xMin;
    public float xMax;


    public float yMin;
    
    // Start is called before the first frame update
    void Start()
    {
        theCountdown = waitingForNextSpawn;
        xMin = -7.2f;
        xMax = 7.2f;
        yMin = 5.1f;
    }

    // Update is called once per frame
    void Update()
    {
        theCountdown -= Time.deltaTime;
        if (theCountdown <= 0)
        {
            spawnStuff();
            theCountdown = waitingForNextSpawn;
        }
    }

    private void spawnStuff()
    {
        Vector2 pos = new Vector2(Random.Range(xMin, xMax), yMin);
        GameObject objectToSpawn = Objects[0];
        Instantiate(objectToSpawn, pos, transform.rotation);
        
    }
}
