using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pitbull_ignoreGround : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ground"))
        {
            Physics2D.IgnoreCollision(collision.otherCollider, collision.collider);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeathZone") || collision.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}
