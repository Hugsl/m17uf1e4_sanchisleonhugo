using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ignoreFireball : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("DeathZone") && collision.otherCollider == this.GetComponent<Collider2D>()) {
            Physics2D.IgnoreCollision(collision.otherCollider, collision.collider);
        }
    }
}
