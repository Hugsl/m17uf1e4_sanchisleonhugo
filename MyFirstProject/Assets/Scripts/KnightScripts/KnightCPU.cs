using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KnightCPU : KnightData
{
    private float dis;
    private RaycastHit2D groundDetectorRight;
    private RaycastHit2D groundDetectorLeft;
    private float time;
    private bool win;


    private enum side
    {
        right, left
    }
    private side lado;
    private state now;
    // Start is called before the first frame update
    void Start()
    {
        lado = side.left;
        dis = 0.75f;
        time = 60f;
        animator.SetFloat("speed", 1);

    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
            move();
        }
        else
        {
            win = false;
            PlayerPrefs.SetString("win", win.ToString());
            SceneManager.LoadScene("FinalSceene");
        }
       
        
    }
    private void move()
    {
        checkSide();
        if (lado == side.right)
        {
            transform.position = new Vector3((-1f * Time.deltaTime) + transform.position.x, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3((1f*Time.deltaTime) + transform.position.x, transform.position.y, transform.position.z);
        }

    }
    private void checkSide()
    {

        groundDetectorRight = Physics2D.Raycast(this.transform.position, Vector2.down,dis);
        groundDetectorLeft = Physics2D.Raycast(this.transform.position, Vector2.down,dis);

        Debug.DrawRay(this.transform.position, Vector2.down*dis);
        Debug.DrawRay(this.transform.position, Vector2.down*dis);


        if (groundDetectorLeft.collider.name != "Ground" && lado == side.right) {

            Debug.Log("I'm in");
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            lado = side.left;
        }
        else if(groundDetectorRight.collider.name != "Ground" && lado == side.left){
            Debug.Log("I'm out");
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            lado = side.right;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {




          

        if (collision.collider.CompareTag("DeathZone"))
        {
            win = true;
            PlayerPrefs.SetString("win", win.ToString());
            SceneManager.LoadScene("FinalSceene");

        }
    }
}
